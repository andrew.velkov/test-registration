import { combineReducers } from 'redux';

import user from './user';
import other from './other';

const reducers = combineReducers({
  user: combineReducers({
    registerData: user('register'),
    signupData: other('signup'),
  }),
});

export default reducers;
