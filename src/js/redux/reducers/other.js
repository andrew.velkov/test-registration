const initialState = {
  data: {
    privatInfo: {
      email: '',
      password: '',
      confirmPassword: '',
    },
    publicInfo: {
      gender: '',
      how_hear_about_us: '',
      dd: '',
      mm: '',
      yyyy: '',
    },
    activeStep: 0,
    progress: 0,
  },
};

export default function other(params = '') {
  return (state = initialState, action = {}) => {
    switch (action.type) {
      case `other/${params}/ADD`:
        return {
          ...state,
          data: action.payload,
        };
      default:
        return state;
    }
  };
}
