import React from 'react';

import SignupContainer from 'modules/SignupContainer';

const Signup = ({ ...props }) => <SignupContainer {...props} />;

export default Signup;
