const SOURCE = [
  {
    label: 'DOU',
    value: 'dou.ua',
  },
  {
    label: 'Rabota.UA',
    value: 'rabota.ua',
  },
  {
    label: 'Linkedin',
    value: 'linkedin.com',
  },
  {
    label: 'Google search',
    value: 'google.com',
  },
  {
    label: 'Radio: XIT FM :)',
    value: 'radio',
  },
  {
    label: 'Other',
    value: 'other',
  },
];

export default SOURCE;
