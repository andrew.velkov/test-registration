const DATE_OF_BIRTH = [
  {
    name: 'dd',
    label: 'DD',
    placeholder: '20',
  },
  {
    name: 'mm',
    label: 'MM',
    placeholder: '02',
  },
  {
    name: 'yyyy',
    label: 'YYYY',
    placeholder: '2000',
  },
];

export default DATE_OF_BIRTH;
