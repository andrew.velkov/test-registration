export const REGISTER_REQUEST = 'user/register/REQUEST';
export const REGISTER_SUCCESS = 'user/register/SUCCESS';
export const REGISTER_FAILURE = 'user/register/FAILURE';
