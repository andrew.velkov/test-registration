import * as type from 'constants/user';

export const register = (data) => ({
  types: [type.REGISTER_REQUEST, type.REGISTER_SUCCESS, type.REGISTER_FAILURE],
  payload: {
    request: {
      url: '/user/register',
      method: 'POST',
      data,
    },
  },
});
