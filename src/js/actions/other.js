import * as type from 'constants/other';

export const signupData = (data) => ({
  type: type.SIGNUP_DATA_ADD,
  payload: data,
});
