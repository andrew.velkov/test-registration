/* eslint-disable no-nested-ternary */
import React from 'react';
import { TextField } from '@material-ui/core';

const Input = ({
  variant = 'outlined',
  select = false,
  label,
  name,
  value,
  onChange,
  error,
  errorText,
  placeholder,
  helperText,
  children,
  ...input
}) => (
  <TextField
    variant={variant}
    select={select}
    label={label}
    name={name}
    value={value}
    fullWidth
    margin="normal"
    placeholder={placeholder}
    error={error}
    helperText={errorText || helperText}
    onChange={onChange}
    {...input}
  >
    {select && children}
  </TextField>
);

export default Input;
