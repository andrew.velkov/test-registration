import React from 'react';
import {
  FormHelperText,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
} from '@material-ui/core';

const RadioGroups = ({ data, title, name, value, onChange, errorText, className }) => (
  <FormControl component="fieldset" error={errorText} fullWidth style={{ paddingBottom: '15px' }}>
    {title && <FormLabel component="legend">{title}</FormLabel>}
    <RadioGroup name={name} value={value} onChange={onChange} className={className}>
      {data.map((item) => (
        <FormControlLabel
          key={item.value || item}
          value={item.value || item}
          label={item.label || item}
          control={<Radio color="primary" />}
        />
      ))}
    </RadioGroup>
    <FormHelperText style={{ paddingTop: '10px' }}>{errorText}</FormHelperText>
  </FormControl>
);
export default RadioGroups;
