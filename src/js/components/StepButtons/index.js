import React from 'react';
import { Button } from '@material-ui/core';

import css from 'styles/components/StepButtons.scss';

const StepButtons = ({
  activeStep, handlePrev, isSubmitting, handleSubmit, nextButtonName,
}) => (
  <ul className={css.buttonGroup}>
    <li className={css.buttonGroup__item}>
      {activeStep > 0 && (
        <Button onClick={handlePrev}>
          Back
        </Button>
      )}
    </li>
    <li className={css.buttonGroup__item}>
      <Button
        type="submit"
        color={nextButtonName ? 'secondary' : 'primary'}
        variant="contained"
        className={css.button}
        disabled={isSubmitting}
        onClick={handleSubmit}
      >
        {nextButtonName || 'Next'}
      </Button>
    </li>
  </ul>
);

export default StepButtons;
