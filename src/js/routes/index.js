import React, { Suspense, lazy } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import ErrorBoundary from 'containers/ErrorBoundary';
import Loader from 'components/Loader';

const App = lazy(() => import('containers/App'));
const Auth = lazy(() => import('containers/Auth'));
const SignupPage = lazy(() => import('pages/Signup'));

const Routes = () => (
  <BrowserRouter>
    <Suspense fallback={<Loader isFetching />}>
      <App>
        <ErrorBoundary>
          <Auth>
            <Switch>
              <Route path="/" render={(props) => <SignupPage {...props} />} />
            </Switch>
          </Auth>
        </ErrorBoundary>
      </App>
    </Suspense>
  </BrowserRouter>
);

export default Routes;
