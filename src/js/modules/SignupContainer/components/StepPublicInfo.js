import React from 'react';
import { MenuItem } from '@material-ui/core';
import cx from 'classnames';

import RadioGroup from 'components/Form/RadioGroups';
import Input from 'components/Form/Input';
import StepButtons from 'components/StepButtons';
import { GENDER, SOURCE, DATE_OF_BIRTH } from 'config';
import Formik from 'helpers/Formik';
import { SignupSchemaStepTwo } from 'helpers/Formik/validation';

import css from 'styles/pages/Home.scss';

const StepPublicInfo = ({
  initialData, handleValidate, activeStep, handlePrev,
}) => (
  <Formik
    initialValues={{ ...initialData }}
    validate={(values) => SignupSchemaStepTwo(values)}
    onSubmit={handleValidate('publicInfo')}
  >
    {({ values, errors, touched, handleChange, handleSubmit, isSubmitting }) => (
      <form className={css.auth__form}>
        <section className={css.auth__section}>
          <h4 className={css.auth__subtitle}>Date of birth</h4>
          <ul className={css.inputGroup}>
            {DATE_OF_BIRTH.map((field) => (
              <li key={field.name} className={css.inputGroup__item}>
                <Input
                  className={cx(css.inputGroup__input, css.auth__input)}
                  label={field.label}
                  name={field.name}
                  variant="outlined"
                  type="number"
                  error={errors[field.name] && touched[field.name]}
                  // errorText={errors[field.name] && touched[field.name] && errors[field.name]}
                  value={values[field.name]}
                  placeholder={field.placeholder}
                  onChange={handleChange}
                />
              </li>
            ))}
          </ul>
          <ul>
            <li className={css.auth__error}>
              <strong>
                {console.log('error')}
                {((errors.mm && touched.mm) || (errors.dd && touched.dd)
                  || (errors.yyyy && errors.dd)) && 'Invalid format date:'}
              </strong>
            </li>
            <li className={css.auth__error}>
              {errors.dd && touched.dd && errors.dd}
            </li>
            <li className={css.auth__error}>
              {errors.mm && touched.mm && errors.mm}
            </li>
            <li className={css.auth__error}>
              {errors.yyyy && touched.yyyy && errors.yyyy}
            </li>
          </ul>
        </section>

        <section className={css.auth__section}>
          <h4 className={css.auth__subtitle}>Gender</h4>
          <RadioGroup
            className={cx(css.radioGroup, css.radioGroup_inline)}
            data={GENDER}
            name="gender"
            // title="Gender"
            error={errors.gender && touched.gender}
            errorText={errors.gender && touched.gender && errors.gender}
            value={values.gender}
            onChange={handleChange}
          />
        </section>

        <section className={css.auth__section}>
          <h4 className={css.auth__subtitle}>Where did you hear about us?</h4>
          <Input
            className={css.auth__input}
            label="Source"
            variant="outlined"
            name="how_hear_about_us"
            select
            error={errors.how_hear_about_us && touched.how_hear_about_us}
            errorText={
              errors.how_hear_about_us && touched.how_hear_about_us && errors.how_hear_about_us
            }
            value={values.how_hear_about_us}
            onChange={handleChange}
          >
            {SOURCE.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </Input>
        </section>

        <StepButtons
          activeStep={activeStep}
          handlePrev={handlePrev}
          isSubmitting={isSubmitting}
          handleSubmit={handleSubmit}
        />
      </form>
    )}
  </Formik>
);

export default StepPublicInfo;
