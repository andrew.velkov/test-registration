import React from 'react';

import Input from 'components/Form/Input';
import StepButtons from 'components/StepButtons';
import Formik from 'helpers/Formik';
import { validateEmailScheme, SignupSchemaStepOne } from 'helpers/Formik/validation';

import css from 'styles/pages/Home.scss';

const StepPrivatInfo = ({
  initialData, handleValidate, activeStep, handlePrev,
}) => (
  <Formik
    initialValues={{ ...initialData }}
    validationSchema={SignupSchemaStepOne}
    validate={(values) => validateEmailScheme(values)}
    onSubmit={handleValidate('privatInfo')}
  >
    {({ values, errors, touched, handleChange, handleSubmit, isSubmitting }) => (
      <form className={css.auth__form}>
        <Input
          className={css.auth__input}
          label="Email"
          variant="outlined"
          type="email"
          name="email"
          error={errors.email && touched.email}
          errorText={errors.email && touched.email && errors.email}
          value={values.email}
          onChange={handleChange}
        />
        <Input
          className={css.auth__input}
          label="Password"
          variant="outlined"
          type="password"
          name="password"
          error={errors.password && touched.password}
          errorText={errors.password && touched.password && errors.password}
          value={values.password}
          onChange={handleChange}
        />
        <Input
          className={css.auth__input}
          label="Confirm Password"
          variant="outlined"
          type="password"
          name="confirmPassword"
          error={errors.confirmPassword && touched.confirmPassword}
          errorText={errors.confirmPassword && touched.confirmPassword && errors.confirmPassword}
          value={values.confirmPassword}
          onChange={handleChange}
        />

        <StepButtons
          activeStep={activeStep}
          handlePrev={handlePrev}
          isSubmitting={isSubmitting}
          handleSubmit={handleSubmit}
        />
      </form>
    )}
  </Formik>
);

export default StepPrivatInfo;
