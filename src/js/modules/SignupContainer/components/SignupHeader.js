import React from 'react';

import css from 'styles/pages/Home.scss';

const SignupHeader = ({ title, progressBar }) => (
  <div className={css.auth__content}>
    <div className={css.auth__progress} style={{ width: `${progressBar}%` }} />
    <h2 className={css.auth__title}>
      {title}
    </h2>
  </div>
);

export default SignupHeader;
