import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { register } from 'actions';
import StepButtons from 'components/StepButtons';
import Fetching from 'components/Fetching';
import { SOURCE } from 'config';

class StepFinishContainer extends Component {
  getInfoFormatter = (publicInfo) => {
    const dateOfBirth = moment(`${publicInfo.mm}-${publicInfo.dd}-${publicInfo.yyyy}`)
    .format('DD.MM.YYYY');

    const howHearAboutUs = publicInfo.how_hear_about_us
    && SOURCE.find((item) => publicInfo.how_hear_about_us.includes(item.value)).label;

    return {
      dateOfBirth,
      dateOfBirthUnix: moment(`${publicInfo.mm}-${publicInfo.dd}-${publicInfo.yyyy}`).unix(),
      howHearAboutUs,
    }
  }

  handleSignup = (e) => {
    e.preventDefault();
    const { registerAction, getSignupData: { privatInfo, publicInfo } } = this.props;
    const { dateOfBirthUnix } = this.getInfoFormatter(publicInfo);

    const createdData = {
      email: privatInfo.email,
      password: privatInfo.password,
      gender: publicInfo.gender,
      date_of_birth: dateOfBirthUnix,
      how_hear_about_us: publicInfo.how_hear_about_us,
    };

    registerAction(createdData);
  }

  getUser = () => {
    const { getRegisterData: { loaded, loading, data, error } } = this.props;
    const userInfo = loaded && data.data.user;
    return {
      userInfo, loaded, loading, error,
    };
  }

  render() {
    const { activeStep, handlePrev, getSignupData } = this.props;
    const { privatInfo } = getSignupData;
    const { userInfo, loaded, loading, error } = this.getUser();
    const { howHearAboutUs } = this.getInfoFormatter(userInfo);

    return (
      <div>
        <Fetching isFetching={loading} bg>
          {(!userInfo && error.msg) && (
            <p style={{ color: 'red' }}>
              { `Error: ${privatInfo.email} - ${error.msg}`}
            </p>
          )}

          {(loaded && userInfo) && (
            <ul>
              <li>{userInfo.email}</li>
              <li>{userInfo.gender}</li>
              <li>{howHearAboutUs}</li>
              <li>{moment(userInfo.date_of_birth * 1000).format('DD/MM/YYYY')}</li>
            </ul>
          )}

          {!loaded && (
            <StepButtons
              activeStep={activeStep}
              handlePrev={handlePrev}
              handleSubmit={this.handleSignup}
              nextButtonName='Send data'
            />
          )}
        </Fetching>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    getSignupData: state.user.signupData.data,
    getRegisterData: state.user.registerData,
  }),
  (dispatch) => ({
    registerAction: (data) => dispatch(register(data)),
  }),
)(StepFinishContainer);
