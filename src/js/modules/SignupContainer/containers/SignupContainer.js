/* eslint-disable react/destructuring-assignment */
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { signupData } from 'actions';
import SignupHeader from '../components/SignupHeader';
import StepPrivatInfo from '../components/StepPrivatInfo';
import StepPublicInfo from '../components/StepPublicInfo';
import StepFinishContainer from './StepFinishContainer';

class SignupContainer extends Component {
  stepPrivatInfo = () => {
    const { getSignupData: { privatInfo, activeStep } } = this.props;

    return (
      <StepPrivatInfo
        initialData={privatInfo}
        handleValidate={this.handleValidate}
        activeStep={activeStep}
        handlePrev={this.handlePrev}
      />
    );
  };

  stepPublicInfo = () => {
    const { getSignupData: { publicInfo, activeStep } } = this.props;

    return (
      <StepPublicInfo
        initialData={publicInfo}
        handleValidate={this.handleValidate}
        activeStep={activeStep}
        handlePrev={this.handlePrev}
      />
    );
  };

  stepFinishContainer = () => {
    const { getSignupData: { activeStep } } = this.props;

    return (
      <StepFinishContainer
        activeStep={activeStep}
        handlePrev={this.handlePrev}
      />
    );
  };

  stepper = () => {
    const stepContent = [
      [this.stepPrivatInfo(), 'Signup (step 1)'],
      [this.stepPublicInfo(), 'Signup (step 2)'],
      [this.stepFinishContainer(), 'Well done, now send us the data :)'],
    ];
    return { stepContent };
  };

  getStepContent = (stepIndex) => {
    const { stepContent } = this.stepper();
    return stepContent[stepIndex];
  };

  handleValidate = (key) => (values) => this.handleNext(key, values);

  handleNext = (key, values) => {
    const { signupDataAction, getSignupData } = this.props;
    const { stepContent } = this.stepper();
    const data = { ...getSignupData };

    if (key && values) data[key] = { ...values };
    data.activeStep += 1;
    data.progress += 100 / (stepContent.length - 1);
    signupDataAction(data);
  };

  handlePrev = () => {
    const { signupDataAction, getSignupData } = this.props;
    const { stepContent } = this.stepper();
    const data = { ...getSignupData };

    data.activeStep -= 1;
    data.progress -= 100 / (stepContent.length - 1);
    signupDataAction(data);
  };

  render() {
    const { getSignupData: { activeStep, progress } } = this.props;
    const progressBar = activeStep > 0 ? progress : 0;

    return (
      <React.Fragment>
        <SignupHeader
          progressBar={progressBar}
          title={this.getStepContent(activeStep)[1]}
        />

        {this.getStepContent(activeStep)[0]}
      </React.Fragment>
    );
  }
}

export default connect(
  (state) => ({
    getSignupData: state.user.signupData.data,
  }),
  (dispatch) => ({
    signupDataAction: (data) => dispatch(signupData(data)),
  }),
)(SignupContainer);
