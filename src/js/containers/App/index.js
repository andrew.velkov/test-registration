import React from 'react';

import css from 'styles/containers/Member.scss';

const App = ({ children }) => (
  <section className={css.member}>
    <article className={css.member__flex}>
      <section className={css.member__wrap}>
        <div className={css.member__container}>
          <section className={css.member__content}>{children}</section>
        </div>
      </section>
    </article>
  </section>
);

export default App;
