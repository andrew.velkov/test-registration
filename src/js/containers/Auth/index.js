import React from 'react';

import css from 'styles/containers/Auth.scss';

const Auth = ({ children }) => (
  <section className={css.auth}>
    <div className={css.auth__wrap}>
      <div className={css.auth__logo} />
      <div className={css.auth__container}>{children}</div>
    </div>
  </section>
);

export default Auth;
