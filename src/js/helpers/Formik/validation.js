import * as Yup from 'yup';
import moment from 'moment';

import strings from 'translations';

export const validateEmailScheme = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = 'Email required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  return errors;
}

export const SignupSchemaStepOne = () =>
  Yup.object().shape({
    password: Yup.string()
      .min(6, strings.formatString(strings.validation.min, { num: 6 }))
      .max(100, strings.formatString(strings.validation.max, { num: 100 }))
      .required(strings.validation.required),
    confirmPassword: Yup.string()
      .min(6, strings.formatString(strings.validation.min, { num: 6 }))
      .max(100, strings.formatString(strings.validation.max, { num: 100 }))
      .oneOf([Yup.ref('password'), null], strings.validation.password_not_match)
      .required(strings.validation.required),
  });

export const SignupSchemaStepTwo = (values) => {

  const errors = {};

  if (!values.gender) {
    errors.gender = strings.validation.required;
  }

  if (!values.dd) {
    errors.dd = `DD: ${strings.validation.required}`;
  } else if (values.dd < 1 || values.dd > 31) {
    errors.dd = `DD: Format 1 - 31`;
  }

  if (!values.mm) {
    errors.mm = `MM: ${strings.validation.required}`;
  } else if (values.mm < 1 || values.mm > 12) {
    errors.mm = `MM: Format 1 - 12`;
  }

  if (!values.yyyy) {
    errors.yyyy = `YYYY: ${strings.validation.required}`;
  } else if (values.yyyy < 1903 || values.yyyy > new Date().getFullYear()) {
    errors.yyyy = `Invalid year format, enter from 1903 - ${new Date().getFullYear()} г.`;
  }

  const { _isValid, _i } = moment(`${values.mm}-${values.dd}-${values.yyyy}`, [
    'MM-DD-YYYY',
    'YYYY-MM-DD',
  ]);
  if (!_isValid) {
    errors.dd = 'Invalid format';
  } else if (_isValid && _isValid && moment().diff(moment(_i), 'years') < 18) {
    errors.yyyy = 'You are not 18 years old';
  }

  return errors;
};
